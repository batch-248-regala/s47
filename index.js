console.log("Hi, B248");
console.log(document); //result - html document
console.log(document.querySelector("#txt-first-name"));
/*
	document - refers to the whole web page
	querySelector - used to select a specific element (obj) as long as it is inside the html tag (HTML ELEMENT)
	- takes a string input that is formatted like CSS Selector
	-can select elements regardless if the string is an id, class, or a tag as long as the element is existing in the webpage
*/

/*	
	Alternative methods that we use aside from querySelector in retrieving alements. 
	Syntax: document.getElementById() 
	document.getElementByClassName() 
	document.getElementByTagName()
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// Function to update full name span
function updateFullName() {
  if (spanFullName) {
    spanFullName.textContent = txtFirstName.value + " " + txtLastName.value;
  }
}

// Event listeners for first name and last name inputs
txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);